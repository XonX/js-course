/*
This is an array. Array is a way to put several different values into one variable
*/
var numbers = [1, 2, 5, 10];
/*
another way to create an array. It's not as commonly used and IDEA will look at you funny if
you do it like this, but for the sake of education I'm going to provide this way of doing
things too.
*/
var anotherArray = new Array(4, 3, 2);

/*
This is how you access the first element of the array. The format is arrayName[indexOfElement]
Note that to acces the first element we use indexOfElement === 0. This is a common agreement between
programmers and programming languages that arrays start at zero. There are, however, languages that didn't
get the memo and start arrays at 1. We usually point and laugh when we see one on the street.
*/
console.log(numbers[0]);
//accessing the 4th element. Index is 3, because we're serius programmers and start out arrays from 0
console.log(numbers[3]);
//print the whole array
console.log(numbers);
//this is how you can find out the length of the array
console.log(numbers.length);

//changing a value of an array element is easy and straightforward
numbers[1] = 50;
console.log(numbers);

/*
you can change the size of an array by just adding an element to the index that is larger then the
current array size. More strict languages won't allow for such nonsence, but JS is chill like that.
If, like in the example below there is a gap between the last element of the array and the new last
element, all the elements in between will be assigned 'undefined' as a value, as it is the default
value for variables.
*/
numbers[7] = 4;
console.log(numbers);

/*
Here is a neat trick to add an element to the end of an array. Since arrays start at 0 (and don't
you forget that), the index of the last element will always be one less then the array's length.
So if we do something like array[array.length] = value, array.length will be a proper index for
adding new element as the last element of an array.
*/
numbers[numbers.length] = 20;
//numbers.length === 8;
//numbers[8] = 20;
console.log(numbers);

/*
You can of course have variables of different types of variables in one array.
It's a party, and everyone's invited!
*/
var party = [4, 6, true, 'hello', null, false];
console.log(party);

//push method pushes the argument provided argument to the end of an array.
//Basically, it's a fancier way to do party[party.length] = 'push it to the limit'
party.push('push it to the limit');
console.log(party);

//pop method returns the last element of an array and removes it from an array
console.log(party.pop());
console.log(party);
/*
If you've ever studied computer science in an university, you might be getting a slight
sense of deja-vu right now. That is because push, pop and their respective behaivour are
standart parts of the Stack data structure. Indeed, via the use of these methods js arrays
can be easily used as stacks. If you don't know what a Stack data structure is, google it
or come ask me questions. It's not important enough for us right now for me to describe it here.
*/

/*
unshift and shift methods are similar to push and pop respectively, but instead of adding or
removing items from the end of an array they do so for the beginning of the array
*/
party.unshift(50);
console.log(party);

console.log(party.shift());
console.log(party);

//do you think you know what this line of code will produce? Go ahead, try it out.
console.log([4, 20, 3, 1, 10].sort());
/*
Surprised? Confused? Good. Welcome to js!
This is, again, a demonstration of how data types in js sometimes don't behave like you
might think they should behave. Because an array can have any datatype in it, the standard
sorting function converts all of the elements to strings and sorts them alphabetically.
Even if the array only contains numbers. This can be bypassed by providing a sorting function as
an argument to the sort() method, but that is way beyond the scope of this lesson.
*/

/*
indexOf function is a way to find the first index of a particular element in an array
*/
console.log(party.indexOf(true));
//if there is no such element in the array, indexOf returns '-1'
console.log(party.indexOf('aaa'));


/*
what follows below is a little demo of how variable scope works in js.
Scope determines if a particular function has access to a particular variable.
The rule is function always has access to the variables that are declared within the function.
In addition it inherents the scope of the place it has been declared it.
*/
var n = 4;
function printNumber() {
    var m = 5;
    function scopeDemo() {
        var x = 9;
        console.log(n + m + x); //all variables in scope, because scopeDemo inherents the scope of printNumber
    }
    //console.log(x) - error. x not in scope
    scopeDemo();
    console.log(n + m); //n in scope because printNumber's scope inherents the global scope
}

//code below will give errors, because it tries to access variables outside it's scope.
// function printString() {
//     console.log(m)
// }
//
// printNumber();
// printString();
// console.log(m);