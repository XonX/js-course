/*
A note on homeworks. I have realised that checking all of the repos you have shared with me is a bit
too overwhelming for me, so I'm switching to a new system for next courses and for the wrap-up of this one.
If you have done a hometask that you want checked but haven't gotten feedback on
1) make sure that the hometask is in a separate folder/repo
2) send link to the folder containing the finished hometask (multiple links for multiple hometasks) to
the following address: juri.kazjulja@gmail.com
Again. One link = one hometask. If I click your link and discover a folder with several hometasks,
I will be very dissapointed in you.

On with the lesson.
I'm not going to provide any in-depth commentary like I usually do, since this lesson didn't really contain
any new material and served more as "bring it all together" type of thing. You are most welcome to write questions
if you have trouble comprehending this or that to the email address above.

The task of the lesson was to write js code for an existing html/css template so that we would get a game.

GAME RULES:

- The game has 2 players, playing in rounds
- In each turn, a player rolls a dice as many times as he whishes. Each result get added to his ROUND score
- BUT, if the player rolls a 1, all his ROUND score gets lost. After that, it's the next player's turn
- The player can choose to 'Hold', which means that his ROUND score gets added to his GLBAL score. After that, it's the next player's turn
- The first player to reach 100 points on GLOBAL score wins the game


HOMEWORK:
3 different versions of this game (only because 2 and 3 aren't easily combinable. You can combine 1 with any of them if you wish)
1) input field for winning score
2) if a player rolls 2 sixes in a row, both current AND global scores are lost
3) add a second dice. Player loses current score if both dice are 1.
*/

var activePlayer, currentScores, globalScores, gameInProgress;

function init() {
    activePlayer = 0;
    currentScores = [0, 0];
    globalScores = [0, 0];
    gameInProgress = true;

    document.querySelector('.player-0-panel').classList.remove('active');
    document.querySelector('.player-1-panel').classList.remove('active');
    document.querySelector('.player-0-panel').classList.remove('winner');
    document.querySelector('.player-1-panel').classList.remove('winner');
    document.querySelector('.player-0-panel').classList.add('active');
    document.getElementById('current-0').textContent = 0;
    document.getElementById('current-1').textContent = 0;
    document.getElementById('score-0').textContent = 0;
    document.getElementById('score-1').textContent = 0;
}

init();

document.querySelector('.btn-roll').addEventListener('click', function () {
    if(gameInProgress) {
        var diceRoll = Math.floor(Math.random() * 6) + 1;
        document.querySelector('.dice').src = 'dice-' + diceRoll + '.png';

        if (diceRoll === 1) {
            updateCurrentScore(0);
            togglePlayer();
        }
        else {
            currentScores[activePlayer] += diceRoll;
            updateCurrentScore(currentScores[activePlayer]);
        }
    }
});

document.querySelector('.btn-hold').addEventListener('click', function () {
    if(gameInProgress) {
        globalScores[activePlayer] += currentScores[activePlayer];
        updateCurrentScore(0);
        document.getElementById('score-' + activePlayer).textContent = globalScores[activePlayer];
        if (globalScores[activePlayer] >= 100) {
            document.querySelector('.player-' + activePlayer + '-panel').classList.add('winner');
            gameInProgress = false;
        }
        else {
            togglePlayer();
        }
    }
});

document.querySelector('.btn-new').addEventListener('click', function () {
    init();
});

function updateCurrentScore(newScore) {
    document.getElementById('current-' + activePlayer).textContent = newScore;
}

function togglePlayer() {
    document.querySelector('.player-0-panel').classList.toggle('active');
    document.querySelector('.player-1-panel').classList.toggle('active');

    currentScores[activePlayer] = 0;
    if (activePlayer === 0) {
        activePlayer = 1;
    }
    else {
        activePlayer = 0;
    }
}