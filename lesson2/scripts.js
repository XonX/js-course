/*
an example of getting input from the user using the "prompt" funcion,
saving it into a variable and using it in an alert afterwards
*/
var userName = prompt("Hi. What is your name?");

alert("Good day, " + userName + "! It's nice to see you!");

//example of using different mathemathical operators
console.log(4 + 5);
console.log(5 - 2);
console.log(5 * 2);
console.log(10 / 2);

/*
examples of operator precedence. * is getting resolved before + because it has higher precedence.
all js operators and their prcedence can be found at 
https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence
*/
console.log(2 + 2 * 2);

var x = 3 + 2 * 9 - 4 / 2;

//brackets () are also an operator. One that has the highest precedence, so everything in the brackets
//will be resolved before anything else
console.log((2 + 2) * 2);

//example of an operator that returns a boolean. + here works as string concactenation operator
//not a mathematical one, because the types of things it is being applied to is not number
var age = 29;
console.log("Is the person underage? " + (age < 18));
// 18 > age - same result

console.log(12 < 12);
console.log(12 <= 12);

var y, z;
/*
this line of code works because of operator associativity. Associativity determines the way in which
operators of the same precedence are parsed. For example if we have a + b + c, it will be resolved as
(a + b) + c, beacause + has left-to-right associativity. The assignment operator = however has right-to-left
associativity, and so the following line is resolved as y = (z = 3) and therefore works ok.
*/
y = z = 3;

console.log(y, z);

//4 = y = z; will not work

var number = 4;
//the plus-equals operator += takes the value of a variable on the left side of it, adds to it the value
//on the right side and assignes the result to the first variable.
//The effect of line below is equivalent to number = number + 5;
number += 5;
console.log(number);

//++ operator adds one to the variable it is applied to
number++;
console.log(number);

//parseInt here is needed because we want to get a number, while prompt returns a string
//parseInt attempts to convert a string to a number
var customerAge = parseInt(prompt("What is the customer's age?"));

/*
Below you see example of the conditional statement. Those statements are very important, so make sure
you know how to use them. You can't do proper programming without them.
Code inside an if block (surrounded by curly braces {}) will only get executed if the expression inside
the braces after the if statement (condition) resolves to true.
Condition of the "else if" statement only gets controlled if condition of the first "if" statement is false.
And code in the else block gets executed if all conditions turn out to be false. Else if and else blocks are
optional. You don't have to provide them with every if statement if you don't need to. Also you can chain however
many else if blocks together as you like.
*/

if (customerAge > 18) {
   alert("Would you like a beer?");
}
//=== operator controls if two values are the same thing. In most programming languages == operator is used
//for that. However in javascript == operator controls if two values "look like" eachother through some type
//conversion magic. For example "18" == 18 would be true, while "18" === 18 would be false. It is strongly
//advised to only use === when controlling for equality, as == may lead to unexpected results, such as
//"true" == true resolving to false
else if (customerAge === 18) {
   alert("May I see some id, please?");
}
else {
   alert("Would you like some juice?");
}

age = 17;

/*
Below, you will find some examples of boolean operators which deal with boolean values and can be very
useful when writing if statements.
&& is the "and" operator. A && B will return true only if both A and B resolve to true and will return false
otherwise.
|| is the "or" operator. A || B will return true if either A or B are true. Essentially, it will only return
false if both A and B are false and will retrun true otherwise.
! is the "not" operator, which "reverses" the boolean value it is applied to. !A will be ture if A is false and
false if A is true.
*/
if (age > 16 && age <= 18) {
    //special rules
}


if (18 > age || age > 80) {
    //other special rules
}

alert(!true);

if(!(age >= 18 && age <= 80)) {
    //same as previous
}
