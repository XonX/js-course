/*
This lesson is mostly theoretical and explains the inner workings of javascript.

ECMAScript (ES1) - 1997
ECMAScript is the name for JS standard and isn't a language of its own.

2009 - ES5 - the most widely supported js standard. If you want to make sure that 
your code runs on most machines, this would be a good standard to choose.

2015 ES6/ES2015 - a major ES release, adding many cool features to the JS
language, as well as switching to an annual release cycle, so that every
year a new ES standard is released. If you're ok with not supporting older systems
annual release can provide a convinient "cut-off" point.
2016 - ES2016

ESnext - new bleeding edge features that aren't part of any standard yet

https://kangax.github.io/compat-table/es6/ - compatibility table you can use to check
if a feature you want to use is supported by your target browser.

What happens when a piece of JS code is getting sent to a JavaScript engine?


1) Parser checks for errors
2) Parser creates an abstract syntax tree which is then translated to machine code
which can be run by a processor
3) Code runs (hopefully)

Execution context - a box, container, wrapper
___________________________
|       f3()              |
|_________________________|
|       f2()              |
|_________________________|
|       f1()              |
|_________________________|
|                         |
| Global execution context|
---------------------------
Execution stack

Every execution context contains the following:
1) Variable object - an object that contains all the variables of the current
context
2) Scope chain - chain of other variable objects the context has access to due
to scope rules
3) "this" variable - what does "this" keyword refer to

Execution context lifecycle
1) Creation phase - Execution context is being prepared
2) Execution phase - this is where the code you wrote actually runs

Creation phase:
1) Create VO:
- Argument object - all the arguments to the function get put into this object
- Functions - code gets scanned for all the function declarations and for every
declaration a variable which points to the function is created. This is why
a function can be called before it is declared
- Variables - for every variable declaration a variable is created in VO and is given the
undifined value

2) Create scope chain

3) Assign value to 'this'
- function call: this = global object
- method call (from an object) - this = object
 */
var object = {
    method: f
};

//while these two functions are the same, 'this' refers to different things because the second function call is tied to an object
f(1, 2);
object.method(1, 2);

function f(a, b) {
    var x;

    x = 3;

    console.log(this)
}

//f2(); will not work, because f2 is a var, not a function, so it's undifined, unless assigned a value

var f2 = function () {
    console.log('can you hear me?')
};

//I've already shown how scope works, but just in case, here's another example
function printNumber() {
    var m = 5;
    function scopeDemo() {
        var x = 9;
        console.log(n + m + x);
    }
    //console.log(x) - error
    scopeDemo();
    console.log(n + m);
}

