var age = 20;

var legalStatus = age > 18 ? 'grownup' : 'underage';
//line above is completely equivalent to the if statement below
// if(age > 18) {
//     legalStatus = 'grownup';
// }
// else {
//     legalStatus = 'underage';
// }

var statusCode = 'V';
var status;

//example of a switch statement. Switch statement executes different code depending on the value
//of the variable provided in the brackets
switch (statusCode) {
    case 'O':
    case 'A':
        status = 'active';
        break;
    case 'B':
    case 'K':
        status = 'blocked';
        break;
    case 'S':
        status = 'in review';
        break;
    default:
        status = 'unknown';
}

console.log(status);

var number = 5;

switch (true) {
    case number < 3:
        console.log('Number is very small');
        break;
    case number < 10:
        console.log('Number is small');
        // break; you might have noticed "break" keywords after every case.
        //this switch can be used to demostrate what happenes if you forget to add them
    default:
        console.log('Number is not small');
}

/*
You have already encountered true and false as boolean values used in if and switch statements.
However, in addition to "hard" true and false, JS also has a concept of "truthy" and "falsy" values.
These are values that are not boolean themeselves, but if put instead of a boolean (in an if
statement, for example) default to true or false. Truthy values default to true, falsy values - to
false.
*/

var someVar;

/*
Here we can see a possible use for the truthy/falsy property of variables. Undefined is a falsy
value, so an if statement like the one below could be used as a check if a particular variable
has been initialized.
*/
if(someVar) {
    console.log('You will not see me');
}

//be aware, however, that 0 is also a falsy value, so if we go
someVar = 0;
//the previous if would still not execute. To avoid this situation a better if is needed.
if(someVar || someVar === 0) {
  console.log('someVar defined as ' + someVar);
}

//falsy: undefined, null, 0, '', NaN
//truthy: everything else

/*
Functions are a very important part of programming, so make sure you understand them well.
Function is a block of code that can be reused by calling the function

Example of a simplest function definition. You can write sayHello(); anywhere in this
file however many times you like and as long as the definition of the function remains
intact, all the code inside the function will be executed.
*/
function sayHello() {
    alert('Hello');
}
/*
Functions can have arguments and return values. Arguments are a way to call the function
with different input data. For example the function below that is used for adding two numbers
can be calles as plus(2, 3); or plus(5, 2); and a and b variables inside the function will be
assigned with respective values.

Return statement indicates that a function returnes a value after it's done. This value then can
be used in the remaining programm. For example
var x = plus(3,4);
will result in x bein equal to 7, as the result of a + b (where a = 3 and b = 4) will be returned
as the result of a function call.
*/
function plus(a, b) {
    return a + b;
}

var result = plus(5, 7);
console.log(result);

//Functions can also call other functions from within themselves
function addTwoNumbers(a, b) {
    console.log(plus(a, b));
}

addTwoNumbers(2, 2);
addTwoNumbers(3, 5);
addTwoNumbers(10, 40);

/*
There is a lot to be said about writing good functions. Robert "Uncle Bob" Martin, author of
"Clean code", "Clean coder" and a series of educational videos on good software architecture
(which I strongly recomend to everyone), provides several guidlines on how to make your functions
nice and readable.

First of all, functions should be small. The rule is to make them small and then try to make them 
even smaller than that. Functions should also do one thing and one thing only. "Thing" could be
a little hard to define, so think of a function like an action. For example, making a payment is
an action, so we can have a makePayment() function for it. However, it is a pretty large action,
which requires validating the payment, sigining it and Core knows what else. Kinda hard to keep
the function small. So we need to separate this huge complex action into smaller actions (again,
as functions) and call them from inside the makePayment() function.
So our makePayment() function may look like the one below, where it is essentialy divided into
sub-actions that are a little more concrete then the huge and nebulous makePayment(). These
sub-actions, in turn, might consist of even more concrete sub-actions until the action you have to make
is so small that you can fit it in a function without further division to subactions.
This way of organising code has 2 major advantages. First, you can always tell, what a function is doing
just by looking at it. And second, it is easy to quickly navigate to a place in the code that you
need to work with. For example, if you want to fix a validation bug, you hop to validate() function, if
you want to change the way signing works to sign() function etc.
Also, be mindful of how you name your functions. Make sure that their name reflects precicely the "thing"
that they are doing. It is also a convention that the first word should be a verb. connectToServer(), saveUserData(),
openFileForWriting() are all examples of good function names.
This also means that plus() function that I defined above is poorly named :)
*/

function makePayment() {
    // validate();
    // sign();
    // debit();
    //whatever else
}