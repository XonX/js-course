//this is also a comment. As you can see, it is a little different then one in html. This is because
//it is a JavaScript comment. It starts with "//" sequence and ends with a newline, so if you want to make
//a multi-line comment you will have to start each new line with // again. This is a little tedious, so js
//has a solution for that.

/*
Much better. This comment can span as many lines as you like until you finish it with "* /" sequence
(without the space, of course. I put a space there so it won't end my comment just yet).

Comments are a tricky subject in programming. As an ideal, you should not have them in code at all.
Code that needs to be commented is code not written well. You should always write code in such a way that a
person reading it afterwards can easily get at least a general idea of what's going on. Only reason I find
it acceptable to put comments in this file is because they serve an educational purpose for all of you
reading this.
 */

/*
log a string of text to the console. To see the result of this code (and other console.log calls found
in this file) you will need to open the index.html file in your browser, open developer tools (F12 key in
all of the browsers I know) and select the "console" tab from there. This is the same console that we log
everything to
 */
console.log('Hello, imported World!');

//this is an example of a variable declaration. A variable is like a box that we store all sorts of data in
//for future retrieval and manipulation
var myVariable = 12;

//variable value retrieval
console.log(myVariable);
//and manipulation
console.log(myVariable + 1);

/*
There are different types of variables in js. One you saw before is a number type variable. Below you
can see a string type variable, that can contain a string of text. Js accepts strings denoted by single
quotes ('') or double quotes ("") and they both operate in exactly the same way. It is an agreed upon convention
in the js world to use single quotes, so do prefer them.
Java, which is my primary language only works with double quoted strings so if you see double quoted strings
in my code that's probably my java instincts taking over.
One last thing to note is that backslash (\) character in the string. This is how we tell js that the following
"'" symbol is not an end of the string but, in fact, a part of it. If the backslash wasn't there then js
would think that the string ends right after "I" and would be very confused about unrecognised words that
come after
 */
var myString = 'I\'m a string!';
//boolean type variables that can either be "true" or "false"
var myBoolean = true;
var myOtherBoolean = false;
//null type variable that pretty much means "no value"
var aNull = null;
//an undefined type variable. Undefined is a default value of any variable that has not been... well...
//defined. As with null it also means "no value" but in a different way. We will touch upon differences
//between null and undefined at a later date.
var someVariable;

//you can declare multiple variables in one string using commas. This string, if uncommented does exactly
//the same things that all of the declarations above
//var myString = 'I\'m a string!', myBoolean = true, myOtherBoolean = false, aNull = null, someVariable;

console.log(myString, myBoolean, myOtherBoolean, aNull, someVariable);

//example of changing variable value AND type
var changingTypes = 3;
changingTypes = 'three';
console.log(changingTypes);

//you can find out the current type of a variable using the typeof operator
console.log(typeof changingTypes);
console.log(typeof 4);

//you can add stings together using the + operator. This line also demonstrates that js will do automatic
//type conversion if it sees fit to do so. In this example the value of the variable myVariable (which is of
//type number) gets converted to type string before + operator is applied to it
console.log(changingTypes + " " + myVariable);

//this line is responsible for that alert window that appears right when you open the page
alert('Hello!');

