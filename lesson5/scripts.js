/*
Now we will look at objects. Objects are a little similar to arrays in that they allow to
put several different values into one variable. The difference is that in an array elements
are guaranteed to be in an order you specified. Objects don't guarantee property order.
Also, objects use property names instead of indexes for accessing and assignment of propery
values
*/
var person = {
    firstName: 'John', //format is propertyName: properyValue
    lastName: 'Smith',
    age: 20, //values can be of different types
    bankAccounts: ['EE2233', 'EE3344'] //you can even use arrays as values
};

//accessing different object properties
console.log(person.firstName, person.lastName);
console.log(person.bankAccounts);

//printing the whole object
console.log(person);
//another way to access a property can be useful if you have a variable for the property name you want
console.log(person['age']);

//case in point
var propToAccess = 'bankAccounts';
console.log(person[propToAccess]);

//you can change property values using both ways of property access
person.age = 30;
console.log(person.age);
person['age'] = 25;
console.log(person.age);

//you can easily add new properties to an object
person.children = 3;
console.log(person);

//another way to make an object. As with a similar way to make an array, not really used
var obj = new Object();
obj['prop'] = 'myProp';
console.log(obj);

var car = {
    manufacturer: 'Volvo',
    year: 2010,
    model: 'V70',
    maxFuel: 50,
    fuelRemaining: 50,
    //did you know, that you can have functions as object properties? Well, you can!
    rideOneHundredKilometers: function () {
        this.fuelRemaining -= 10; //'this' keyword is what you use to refer to the object from which you're calling the function
                                  //the variables of the object aren't in scope of the function beloning to the object so 'this'
                                  //keyword is required
    },
    displayFuelUsage: function () {
        return this.maxFuel - this.fuelRemaining;
    }
};

//vroom-vroom
console.log(car.displayFuelUsage());
car.rideOneHundredKilometers();
console.log(car.displayFuelUsage());
car.rideOneHundredKilometers();
car.rideOneHundredKilometers();
console.log(car.displayFuelUsage());

console.log('-------------');

/*
And now, we have reached loops. Loops are a good way to avoid code duplication. There are a lot of problems in
programming that require to perform an operation, or a set of operations several times or on several data elements.
Loops help us to specify the operation once and execute it however many times we want.
*/
for (var i = 0; i < 10; i++) {
  console.log(i);
}
/*
let's unpack this loop. First, we have the 'for' keyword, that instructs js that we want to make a 'for loop'.
Syntax of the for loop is as follows:
for (statement 1; statement 2; statement 3) {
  some code here
}
where statement 1 is executed once before the loop starts. In the example above statement 1 is 'var i = 0', so it
creates a variable that we will use in the loop and sets it to 0. Statement 2 defines the condition for loop continuation.
Loop will continue for one more iteration if and only if statement 2 resolves to true. In our example statement 2 is 'i < 10'
which means that the loop will run as long as i is smaller then 10. Statement 3 is executed after each loop iteration. In
our example it means that i gets incremented after every iteration
*/

/*
an example of using a loop to add 3 to every array element
*/
var array = [4, 2, 88, 29, 7];
for (var j = 0; j < array.length; j++) {
  array[j] += 3;
}
console.log(array);

var x = 2;
for (; x < 5; ) { //all of the statements in the for loop declaration can be ommited, as we are doing here. It's not an often
                  //used trick, but it's still useful to know that you can do it
  console.log(x);
  x++;
}

/*
while is a simpler version of a loop that you only provide the condition statement to. It's similar to a for loop where only
the second statement is specified
*/
x = 2;
while (x < 5) {
  console.log(x);
  x++;
}